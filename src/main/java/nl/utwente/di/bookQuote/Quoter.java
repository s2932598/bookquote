package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> books = new HashMap<>();
    public Quoter() {
        books.put("1",10.0);
        books.put("2",45.0);
        books.put("3",20.0);
        books.put("4", 50.0);
    }
    double getBookPrice(String isbn) {

        if (books.get(isbn) != null) {
            return books.get(isbn);
        } else {
            return 0;
        }
    }
}
